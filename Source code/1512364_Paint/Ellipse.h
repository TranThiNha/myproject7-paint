#pragma once
#include "Shape.h"
class CEllipse :
	public CShape
{
public:
	CEllipse();
	~CEllipse();
	CEllipse(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(HDC hdc) {
		Ellipse(hdc, x1, y1, x2, y2);
	}

	CShape* Create(int a, int b, int c, int d) {
		return new CEllipse(a, b, c, d);
	}
};

