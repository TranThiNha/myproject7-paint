// 1512364_Paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512364_Paint.h"
#include "Shape.h"
#include "Line.h"
#include "Rectangle.h"
#include "Square.h"
#include "Ellipse.h"
#include "Circle.h"
#include <vector>
#include <windowsx.h>

using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY1512364_PAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512364_PAINT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1512364_PAINT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1512364_PAINT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, L"Paint", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//

int chooseMenu = 0;

vector<CShape*> shapes;
vector<CShape*> prototypes;
int startX;
int startY;
int lastX;
int lastY;
bool isDrawing = false;
CShape* shapeTemp;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HMENU hMenu = GetMenu(hWnd);
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_LBUTTONDOWN:
	{
						   startX = GET_X_LPARAM(lParam);
						   startY = GET_Y_LPARAM(lParam);
							isDrawing = true;
						   break;
	}
	case WM_MOUSEMOVE: {
							   if (isDrawing) {
								   lastX = GET_X_LPARAM(lParam);
								   lastY = GET_Y_LPARAM(lParam);

								   InvalidateRect(hWnd, NULL, TRUE);
							   }
						   break;
	}
	case WM_LBUTTONUP:{
						  lastX = GET_X_LPARAM(lParam);
						  lastY = GET_Y_LPARAM(lParam);
						  
						  CShape* temp;
						  temp = prototypes[chooseMenu]->Create(startX, startY, lastX, lastY);

						  shapes.push_back(temp);
						  isDrawing = false;

						  InvalidateRect(hWnd, NULL, TRUE);
	}
		break;
	case WM_KEYDOWN:
	{
					   if (wParam == VK_SHIFT)
					   {
						   if (chooseMenu == 1)
						   {
							   chooseMenu = 2;
						   }
						   else if (chooseMenu == 3)
						   {
							   chooseMenu = 4;
						   }
					   }
					   break;
	}
	case WM_KEYUP:
	{
					 if (wParam == VK_SHIFT)
					 {
						 if (chooseMenu == 1)
						 {
							 chooseMenu = 2;
						 }
						 else if (chooseMenu == 3)
						 {
							 chooseMenu = 4;
						 }
					 }
					 break;
	}
	case WM_CREATE:
	{
					  prototypes.push_back(new CLine());
					  prototypes.push_back(new CRectangle());
					  prototypes.push_back(new CSquare());
					  prototypes.push_back(new CEllipse());
					  prototypes.push_back(new CCircle());
					  break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_SHAPE_LINE:
		{
							  if (chooseMenu == 1) CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_UNCHECKED);
							  if (chooseMenu == 2) CheckMenuItem(hMenu, ID_SHAPE_SQUARE, MF_UNCHECKED);
							  if (chooseMenu == 3) CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_UNCHECKED);
							  if (chooseMenu == 4) CheckMenuItem(hMenu, ID_SHAPE_CIRCLE, MF_UNCHECKED);
							  CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_CHECKED);
							  chooseMenu = 0;
							  break;
		}
		case ID_SHAPE_RECTANGLE:
		{
								   if (chooseMenu == 0) CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_UNCHECKED);
								   if (chooseMenu == 2) CheckMenuItem(hMenu, ID_SHAPE_SQUARE, MF_UNCHECKED);
								   if (chooseMenu == 3) CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_UNCHECKED);
								   if (chooseMenu == 4) CheckMenuItem(hMenu, ID_SHAPE_CIRCLE, MF_UNCHECKED);
								   CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_CHECKED);
								   chooseMenu = 1;
								   break;
		}
		case ID_SHAPE_SQUARE:
		{
								if (chooseMenu == 1) CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_UNCHECKED);
								if (chooseMenu == 0) CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_UNCHECKED);
								if (chooseMenu == 3) CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_UNCHECKED);
								if (chooseMenu == 4) CheckMenuItem(hMenu, ID_SHAPE_CIRCLE, MF_UNCHECKED);
								CheckMenuItem(hMenu, ID_SHAPE_SQUARE, MF_CHECKED);
								chooseMenu = 2;
								break;
		}
		case ID_SHAPE_ELLIPSE:
		{
								 if (chooseMenu == 1) CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_UNCHECKED);
								 if (chooseMenu == 2) CheckMenuItem(hMenu, ID_SHAPE_SQUARE, MF_UNCHECKED);
								 if (chooseMenu == 0) CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_UNCHECKED);
								 if (chooseMenu == 4) CheckMenuItem(hMenu, ID_SHAPE_CIRCLE, MF_UNCHECKED);
								 CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_CHECKED);
								 chooseMenu = 3;
								 break;
		}
		case ID_SHAPE_CIRCLE:
		{
								if (chooseMenu == 1) CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_UNCHECKED);
								if (chooseMenu == 2) CheckMenuItem(hMenu, ID_SHAPE_SQUARE, MF_UNCHECKED);
								if (chooseMenu == 3) CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_UNCHECKED);
								if (chooseMenu == 0) CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_UNCHECKED);
								CheckMenuItem(hMenu, ID_SHAPE_CIRCLE, MF_CHECKED);
								chooseMenu = 4;
								break;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		
		for (int i = 0; i < shapes.size(); i++) {
			shapes[i]->Draw(hdc);
		}

		if (isDrawing) {
			if (shapeTemp != NULL) delete shapeTemp;
			shapeTemp = prototypes[chooseMenu]->Create(startX, startY, lastX, lastY);
			shapeTemp->Draw(hdc);
		}
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
