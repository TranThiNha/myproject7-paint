#pragma once
#include "Shape.h"
class CCircle :
	public CShape
{
public:
	CCircle();
	~CCircle();
	CCircle(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(HDC hdc) {
		int min;
		if (abs(x2 - x1) > abs(y2 - y1))
		{
			min = abs(x2 - x1);
		}
		else min = abs(y2 - y1);
		if (x2 > x1 && y2 > y1)
			Ellipse(hdc, x1, y1, x1 + min, y1 + min);
		else if (x2 > x1 && y2 < y1)
			Ellipse(hdc, x1, y1, x1 + min, y1 - min);
		else if (x2 < x1 && y2 < y1)
			Ellipse(hdc, x1, y1, x1 - min, y1 - min);
		else
			Ellipse(hdc, x1, y1, x1 - min, y1 - min);
	}

	CShape* Create(int a, int b, int c, int d) {
		return new CCircle(a, b, c, d);
	}
};

