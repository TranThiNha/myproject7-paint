#pragma once
#include "Shape.h"
class CRectangle :
	public CShape
{
public:
	CRectangle();
	~CRectangle();
	CRectangle(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(HDC hdc) {
		Rectangle(hdc, x1, y1, x2, y2);
	}

	CShape* Create(int a, int b, int c, int d) {
		return new CRectangle(a, b, c, d);
	}

};

